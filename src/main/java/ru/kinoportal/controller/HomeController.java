package ru.kinoportal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.kinoportal.model.Films;
import ru.kinoportal.service.FilmsService;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Copyright (c)
 *  @author: Roman Dey
 *  @date 22.07.2020, 14:37
 */

@Controller
@RequestMapping("/")
public class HomeController implements ErrorController {

    @Autowired
    private FilmsService filmsService;

    @RequestMapping(method = {
            RequestMethod.GET,
            RequestMethod.HEAD
    })

    public String showIndex(ModelMap model) {
        List<Films> filmsList = filmsService.findAll();
        model.addAttribute("films", filmsList);
        return "index";
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
