package ru.kinoportal.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.kinoportal.model.Films;
import ru.kinoportal.repository.FilmsRepository;

import javax.persistence.Cacheable;
import java.util.List;

/**
 * Copyright (c)
 *  @author: Roman Dey
 *  @date 22.07.2020, 14:43
 */
@Service
public class FilmsService {
    @Autowired
    @Qualifier("filmsRepository")
    private FilmsRepository filmsRepository;

    public void save(Films films) {
        filmsRepository.save(films);
    }

    public List<Films> findAll() {
        return (List<Films>) filmsRepository.findAll();
    }

}
