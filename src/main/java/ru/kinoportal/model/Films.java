package ru.kinoportal.model;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import javax.persistence.*;

/**
 * Copyright (c)
 *  @author: Roman Dey
 *  @date 22.07.2020, 14:45
 */
@Entity
@Table(name = "films")
public class Films {
    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    @Column(name = "id")
    private long id;

    @Column(name = "title", length = 512)
    private String title;

    @Column(name = "short_story")
    private String short_story;

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getShortStory() {
        return short_story;
    }
}
