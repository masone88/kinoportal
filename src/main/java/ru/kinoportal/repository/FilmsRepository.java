package ru.kinoportal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.kinoportal.model.Films;

/**
 * Copyright (c)
 *  @author: Roman Dey
 *  @date 22.07.2020, 14:51
 */
@Repository

public interface FilmsRepository extends CrudRepository<Films, Long>
{
}
