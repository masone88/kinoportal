package ru;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Copyright (c)
 *  @author: Roman Dey
 *  @date 22.07.2020, 14:38
 */
@SpringBootApplication
public class PortalApp {
    public static void main(String[] args) {
        SpringApplication.run(PortalApp.class, args);
    }
}
